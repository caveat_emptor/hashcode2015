package hashcode2015;

import java.util.ArrayList;
import java.util.Collections;


public class DataCenter
{
	private ArrayList<Row> listOfRow;
	private ArrayList<Server> listOfServer;
	private ArrayList<Pool> listOfPools;
	
	public DataCenter(int numRows, int numServers, int numPools)
	{
		setListOfRow(new ArrayList<Row>(numRows));
		listOfServer = new ArrayList<Server>(numServers);
		listOfPools = new ArrayList<Pool>(numPools);
	}
	
	public void addToRows(Row row)
	{
		listOfRow.add(row);
	}
	
	public void addServer(Server server)
	{
		listOfServer.add(server);
	}
	public ArrayList<Row> getListOfRow()
	{
		return listOfRow;
	}

	public void setListOfRow(ArrayList<Row> listOfRow)
	{
		this.listOfRow = listOfRow;
	}
	
	
	public void addPool(Pool pool)
	{
		listOfPools.add(pool);
	}
	
	public ArrayList<Server> sortServer() 
	{
		ArrayList<Server> sortedServers = (ArrayList<Server>)this.listOfServer.clone();
		Collections.sort(sortedServers, new ServerComparatorDescending());
		return sortedServers;
	}
	public ArrayList<Server> sortServerAsc() 
	{
		ArrayList<Server> sortedServers = (ArrayList<Server>)this.listOfServer.clone();
		Collections.sort(sortedServers, new ServerComparatorAscending());
		return sortedServers;
	}
	
	public void allocateServersToPools()
	{
		int index = 0;
		for (Server server : sortServer())
		{
			listOfPools.get(index).addServer(server);
			server.setPool(index);
			index++;
			
			if (index == listOfPools.size())
				index = 0;
		}
	}
	
	public void allocateServerLocation()
	{
		boolean once = true;
		int index = 0;
		for (Server server : sortServerAsc())
		{
			int slot =listOfRow.get(index).isAllSlotsNotBroken(0, server.getSize());
			while(slot == -1)
			{
				index++;
				if(index == listOfRow.size())
				{
					index = 0;
					if(once == false)
						once = true;
					else
						return;
				}
				slot = listOfRow.get(index).isAllSlotsNotBroken(0, server.getSize());
			}
			once = false;

			listOfRow.get(index).addServerToNextAvailableSlot(server);
			server.setRow(index);
			index++;
			
			if (index == listOfRow.size())
				index = 0;
		}
	}
	
	public String toString()
	{
		String output = "";
		for(Server srvr : listOfServer){
			if(srvr.getRow() == -1){
				output += "x\n";
				continue;
			}
			output += srvr.getRow() + " ";
			output += srvr.getStartSlot() + " ";
			output += srvr.getPool() + "\n";
			
		}
		return output;
		
		
	}
	
	public void printValue()
	{
		ArrayList<Integer> values = new ArrayList<Integer>(listOfPools.size());
		for(int i = 0; i < listOfPools.size(); i++)
		{
			System.out.println(i + ": " + listOfPools.get(i).getSum());
			values.add(listOfPools.get(i).getSum());
			
		}
		System.out.println("Minimum = " + Collections.min(values));
	}
}
