package hashcode2015;

import java.util.Comparator;

public class ServerComparatorAscending  implements Comparator<Server>
{
	@Override
	public int compare(Server server1, Server server2)
	{
		//ascending
		return (int)(100*(server1.density - server2.density));
		
		//descending
//		return (int)(100*(server2.density - server1.density));
	}
}
