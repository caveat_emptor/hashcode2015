package hashcode2015;

import java.io.FileNotFoundException;
import java.io.PrintWriter;


public class Main
{
	public static void main(String[] args) throws FileNotFoundException
	{
		ObjectFactory fr = new ObjectFactory("dc.in");
		DataCenter dc = fr.parseFile();
		dc.allocateServersToPools();
		dc.allocateServerLocation();
		
		//PrintWriter pw = new PrintWriter("dc.out");
		//pw.print(dc);
		//pw.close();
		
		dc.printValue();
	}

}
