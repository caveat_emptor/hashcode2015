package hashcode2015;

public class Server
{
	private int row;
	private int size;
	private int capacity;
	private int startSlot;
	private int pool;
	public double density;
	
	public Server(int size, int capacity, int row, int startSlot)
	{
		this.setRow(row);
		this.setSize(size);
		this.setCapacity(capacity);
		this.setStartSlot(startSlot);
		density = (double)capacity / (double)size; 
	}
	
	public int getRow()
	{
		return row;
	}
	
	public void setRow(int row)
	{
		this.row = row;
	}
	
	public int getSize()
	{
		return size;
	}
	
	public void setSize(int size)
	{
		this.size = size;
	}
	
	public int getCapacity()
	{
		return capacity;
	}
	
	public void setCapacity(int capacity)
	{
		this.capacity = capacity;
	}
	
	public int getStartSlot()
	{
		return startSlot;
	}
	
	public void setStartSlot(int startSlot)
	{
		this.startSlot = startSlot;
	}
	
	public int getPool()
	{
		return pool;
	}
	
	public void setPool(int pool)
	{
		this.pool = pool;
	}
	

}
