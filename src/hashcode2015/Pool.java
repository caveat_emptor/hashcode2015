package hashcode2015;

import java.util.ArrayList;

public class Pool
{
	private ArrayList<Server> servers;
	
	public Pool()
	{
		setServers(new ArrayList<Server>());
	}
	
	public void addServer(Server server)
	{
		servers.add(server);
	}

	public ArrayList<Server> getServers()
	{
		return servers;
	}

	public void setServers(ArrayList<Server> servers)
	{
		this.servers = servers;
	}
	
	public int getSum()
	{
		int sum = 0;
		for(int i = 0; i < servers.size(); i++)
		{
			sum += servers.get(i).getCapacity();
		}
		return sum;
	}
}
