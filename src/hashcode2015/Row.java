package hashcode2015;

import java.util.ArrayList;


public class Row
{
	private ArrayList<Slot> listOfSlots;
	private int currentSlot;
	
	public Row(int size)
	{
		currentSlot = 0;
		setListOfSlots(new ArrayList<Slot>(size));
	}
	
	public boolean addServerToNextAvailableSlot(Server server)
	{		
		for(int i = 0; i < server.getSize(); i++)
		{
			listOfSlots.get(currentSlot+i).setFilled(true);
		}
		server.setStartSlot(currentSlot);
		currentSlot += server.getSize();
		
		return true;
	}
	
	public int isAllSlotsNotBroken(int startSlot, int serverSize)
	{
		for(int i = 0; i < serverSize; i++)
		{
			if(startSlot >= listOfSlots.size())
			{
				return -1;
			}
			if(listOfSlots.get(startSlot).isBroken() || listOfSlots.get(startSlot).isFilled())
			{
				return isAllSlotsNotBroken(++startSlot, serverSize);
			}
			startSlot++;
		}
		
		return startSlot;
	}
	public void addToSlots(Slot slot)
	{
		listOfSlots.add(slot);
	}
	
	public ArrayList<Slot> getListOfSlots()
	{
		return listOfSlots;
	}
	
	public void setListOfSlots(ArrayList<Slot> listOfSlots)
	{
		this.listOfSlots = listOfSlots;
	}

	public int getCurrentSlot()
	{
		return currentSlot;
	}

	public void setCurrentSlot(int currentSlot)
	{
		this.currentSlot = currentSlot;
	}
	
}
