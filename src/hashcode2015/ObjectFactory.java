package hashcode2015;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class ObjectFactory {
	private File fileInput;
	
	public ObjectFactory(String filename)
	{
		fileInput = new File(filename);
		if(!fileInput.exists())
			System.out.println("Cannot find file. Make sure you download the dc.in file and copied it to working directory.");
	}
	
	public DataCenter parseFile()
	{
		try {
			BufferedReader in = new BufferedReader(new FileReader(fileInput.getName()));
			StringTokenizer st = new StringTokenizer(in.readLine());
			int rows = Integer.parseInt(st.nextToken());
			int slots = Integer.parseInt(st.nextToken());
			int brokens = Integer.parseInt(st.nextToken());
			int pools = Integer.parseInt(st.nextToken());
			int servers = Integer.parseInt(st.nextToken());
			
			//instantiate data center
			DataCenter dc = new DataCenter(rows, servers, pools);
			//instantiate row object
			for(int i = 0; i < rows; i++)
			{
				Row row = new Row(slots);
				dc.addToRows(row);
				//instantiate slot object
				for(int j = 0; j < slots; j++)
				{
					row.addToSlots(new Slot(false));
				}
			}
			
			//reading and marking broken slots
			for(int i = 0; i < brokens; i++)
			{
				st = new StringTokenizer(in.readLine());
				int row = Integer.parseInt(st.nextToken());
				int slot = Integer.parseInt(st.nextToken());
				dc.getListOfRow().get(row).getListOfSlots().get(slot).setBroken(true);
			}
			
			//adding servers
			for(int i = 0; i < servers; i++)
			{
				st = new StringTokenizer(in.readLine());
				int size = Integer.parseInt(st.nextToken());
				int capacity = Integer.parseInt(st.nextToken());
				dc.addServer(new Server(size, capacity, -1, -1));
			}
			
			//adding pools
			for(int i = 0; i < pools; i++)
			{
				dc.addPool(new Pool());
			}
			return dc;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
