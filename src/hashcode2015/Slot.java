package hashcode2015;

public class Slot
{
	private boolean isFilled;
	private boolean isBroken;
	
	public Slot(boolean broken)
	{
		setFilled(false);
		setBroken(broken);
	}

	public boolean isFilled()
	{
		return isFilled;
	}

	public void setFilled(boolean isFilled)
	{
		this.isFilled = isFilled;
	}

	public boolean isBroken()
	{
		return isBroken;
	}

	public void setBroken(boolean isBroken)
	{
		this.isBroken = isBroken;
	}
	
}
